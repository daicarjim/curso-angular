If you have issues of installations for permissions then apply the next command:

npm install -g @angular/cli --unsafe-perm=true --allow-root

WARNING:

If you want run angular in docker then when you create the serve into the container apply the next command:

ng serve --host 0.0.0.0

The only way that your application listen in port:4200 in your browser.
